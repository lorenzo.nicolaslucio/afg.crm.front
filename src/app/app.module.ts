import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { InMemoryDb } from './customer/services/inMemoryDb.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    environment.production ?
      [] : HttpClientInMemoryWebApiModule.forRoot(InMemoryDb, {delay: 1500})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
