import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from '../models/Customer';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  customer!: Customer;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.customer = history.state;

    if (!this.customer || !this.customer.lastName) {
      this.router.navigate(['/customers']);
    }
  }

  back(): void {
    this.router.navigate(['/customers']);
  }
}
