export class Customer {
    salutation = '';
    firstName = '';
    lastName = '';
    addressLine1 = '';
    addressLine2 = '';
    city = '';
    state = '';
    zipCode = '';
    emailAddress = '';
    telePhoneNumber = '';
}
