import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Customer } from '../models/Customer';

export class InMemoryDb implements InMemoryDbService {

  createDb() {
    const customers: Customer[] = [
      {
        salutation: 'Mr.',
        firstName: 'Paul',
        lastName: 'Mitchell',
        addressLine1: '1222 56th St S',
        addressLine2: 'apt 23',
        city: 'Fargo',
        state: 'ND',
        zipCode: '58104',
        emailAddress: 'ash@sample.com',
        telePhoneNumber: '5551345699',
      },
      {
        salutation: 'Mr.',
        firstName: 'Nick',
        lastName: 'Johnsonn',
        addressLine1: '3000 20th St S',
        addressLine2: '',
        city: 'Nashville',
        state: 'TN',
        zipCode: '37015',
        emailAddress: 'nick@johnsonn.com',
        telePhoneNumber: '5551234567',
      }
    ];

    return { customers };
  }
}
