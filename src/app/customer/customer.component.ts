import { Component, OnInit } from '@angular/core';
import { Customer } from './models/Customer';
import { CustomerService } from './services/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  customers: Customer[] = [];
  loading = false;

  constructor(private customerService: CustomerService) { }

  ngOnInit(): void {
    this.loading = true;
    this.customerService.getCustomers().subscribe(
      (response) => {
        this.customers = response;
        this.loading = false;
      },
      (error) => {
        // TODO handle errors
        this.loading = false;
      }
    );
  }
}
